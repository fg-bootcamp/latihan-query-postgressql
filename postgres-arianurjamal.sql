create database bootcamp_arianurjamal;


create schema blog;

CREATE table posts (
	id int primary key,
	title varchar(25),
	published_data timestamp,
	content text,
	user_id varchar,
	category_id varchar,
	constraint post_pk unique (id)
);

select  * from blog.posts

CREATE TYPE user_role AS ENUM ('admin', 'user');

create table users (
	id int primary key,
	name varchar,
	email varchar(25),
	password varchar(255),
	role user_role,
	constraint users_pk unique (id)
)

select  * from blog.users

create table categories (
	id varchar primary key,
	name varchar(15),
	constraint categories_pk unique (id)
)

select  * from blog.categories;


insert  into blog.users  (id, name, email, password, role)
values (1, 'aria', 'aria@example.com', 'bootcampfg', 'admin')

insert into blog.categories (id, name)
values ('tech', 'tech'), ('bsn', 'business')


insert into blog.posts (id, title, published_data, content, user_id, category_id)
values (1, 'Bootcamp backend', null, 'Ini adlah tugas bootcamp backend nodejs', null, null)

update blog.posts 
set user_id = 1
where id = 1


update blog.posts 
set category_id  = 'tech'
where id = 1